//
//  NXTBarCodeReader.m
//  NYXOPT
//
//  Created by o.koval on 07.09.15.
//  Copyright (c) 2015 o.koval. All rights reserved.
//

#import "NXTBarCodeReader.h"

@implementation NXTBarCodeReader

+ (BOOL)supportsMetadataObjectTypes:(NSArray *)metadataObjectTypes
{
    if (![self isAvailable])
    {
        return NO;
    }
    
    @autoreleasepool
    {
        AVCaptureDevice *captureDevice    = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:nil];
        AVCaptureMetadataOutput *output   = [[AVCaptureMetadataOutput alloc] init];
        AVCaptureSession *session         = [[AVCaptureSession alloc] init];
        [session addInput:deviceInput];
        [session addOutput:output];
        
        if (metadataObjectTypes == nil || metadataObjectTypes.count == 0)
        {
            // Check the QRCode metadata object type by default
            metadataObjectTypes = @[AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode];
        }
        for (NSString *metadataObjectType in metadataObjectTypes)
        {
            if (![output.availableMetadataObjectTypes containsObject:metadataObjectType])
            {
                return NO;
            }
        }
        
        return YES;
    }
}

@end
