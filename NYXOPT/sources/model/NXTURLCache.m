//
//  NXTURLCache.m
//  
//

#import "NXTURLCache.h"
@import Foundation;

@interface NXTURLCache()

@property (strong, nonatomic) NSMutableDictionary *cacheMap;
@property (strong, nonatomic) NSMutableDictionary *memoryCache;

@end

@implementation NXTURLCache

- (id)initWithMemoryCapacity:(NSUInteger)memoryCapacity diskCapacity:(NSUInteger)diskCapacity diskPath:(NSString *)path
{
    self = [super initWithMemoryCapacity:memoryCapacity diskCapacity:diskCapacity diskPath:path];
    if (nil != self)
    {
        NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/FileMap"];
        self.cacheMap = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
        self.memoryCache = [NSMutableDictionary new];
        if (nil == self.cacheMap)
        {
            self.cacheMap = [NSMutableDictionary new];
        }
        NSString *cachePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/GameCache"];
        [[NSFileManager defaultManager] createDirectoryAtPath:cachePath withIntermediateDirectories:YES attributes:nil error:NULL];
    }
    return self;
}

- (NSString *)keyForRequest:(NSURLRequest *)request
{
    NSString *url = request.URL.absoluteString;
    NSString *method = request.HTTPMethod;
    NSString *headers = [request.allHTTPHeaderFields.allValues componentsJoinedByString:@"."];
    return [NSString stringWithFormat:@"%@_%@_%@", url, method, headers];
}

- (NSCachedURLResponse *)cachedResponseForRequest:(NSURLRequest *)request
{
    NSString *key = [self keyForRequest:request];
    NSString *fileName = self.cacheMap[key];
    if (nil != fileName)
    {
        NSString *filePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/GameCache/"];
        filePath = [filePath stringByAppendingPathComponent:fileName];
        NSCachedURLResponse *response = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
        self.memoryCache[key] = response;
        return response;
    }
    return nil;
}

- (void)saveMap
{
    NSData *dataToSave = [NSPropertyListSerialization dataWithPropertyList:self.cacheMap format:NSPropertyListBinaryFormat_v1_0 options:0 error:NULL];
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/FileMap"];
    [dataToSave writeToFile:path atomically:YES];
}

- (void)storeCachedResponse:(NSCachedURLResponse *)cachedResponse forRequest:(NSURLRequest *)request
{
    NSString *key = [self keyForRequest:request];
    NSString *fileName = self.cacheMap[key];
    if (nil == fileName)
    {
        fileName = [[NSUUID new] UUIDString];
        self.cacheMap[key] = fileName;
        [self saveMap];
    }
    NSString *filePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/GameCache/"];
    filePath = [filePath stringByAppendingPathComponent:fileName];
    BOOL result = [NSKeyedArchiver archiveRootObject:cachedResponse toFile:filePath];
    NSAssert(result, @"");
}

- (NSUInteger)currentMemoryUsage
{
    return 0;
}

- (NSUInteger)memoryCapacity
{
    return 0;
}

- (NSUInteger)diskCapacity
{
    return NSUIntegerMax;
}

- (NSUInteger)currentDiskUsage
{
    return 1234;
}


@end
