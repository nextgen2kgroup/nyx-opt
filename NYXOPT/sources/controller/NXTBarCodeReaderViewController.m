//
//  NXTBarCodeReaderViewController.m
//  NYXOPT
//
//  Created by o.koval on 07.09.15.
//  Copyright (c) 2015 o.koval. All rights reserved.
//

#import "NXTBarCodeReaderViewController.h"
#import "NXTBarCodeReader.h"

@implementation NXTBarCodeReaderViewController

- (id)initWithCancelButtonTitle:(NSString *)cancelTitle
{
    return [self initWithCancelButtonTitle:cancelTitle metadataObjectTypes:@[AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode]];
}

- (id)initWithCancelButtonTitle:(NSString *)cancelTitle metadataObjectTypes:(NSArray *)metadataObjectTypes
{
    NXTBarCodeReader *reader = [NXTBarCodeReader readerWithMetadataObjectTypes:metadataObjectTypes];
    return [self initWithCancelButtonTitle:cancelTitle codeReader:reader];
}
@end
