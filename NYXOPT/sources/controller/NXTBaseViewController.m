//
//  NXTBaseViewController.m
//  NYXOPT
//
//  Created by o.koval on 24.09.15.
//  Copyright (c) 2015 o.koval. All rights reserved.
//

#import "NXTBaseViewController.h"

@interface NXTBaseViewController ()

@end

@implementation NXTBaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

@end
