//
//  NXTWebViewController.m
//  NYXOPT
//
//  Created by o.koval on 08.09.15.
//  Copyright (c) 2015 o.koval. All rights reserved.
//

#import "NXTWebViewController.h"
#import "Masonry.h"
#import "NXTURLCache.h"

static const NSInteger kKB = 1024;
static const NSInteger kMB = 1024 * kKB;
static const NSInteger kGB = 1024 * kMB;

@interface NXTWebViewController () <UIWebViewDelegate>
@property (strong, nonatomic) UIWebView *webView;
@end

@implementation NXTWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.webView = [UIWebView new];
    self.webView.delegate = self;
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://play.famobi.com/smarty-bubbles"]]];
    [self.view addSubview:self.webView];
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topLayoutGuide);
        make.leading.equalTo(self.view);
        make.trailing.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    NSLog(@"%@", [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]);
    NXTURLCache *cache = [[NXTURLCache alloc] initWithMemoryCapacity:kMB * 50 diskCapacity:kGB diskPath:@"TestPath/"];
    [NSURLCache setSharedURLCache:cache];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSURL *url = [NSURL URLWithString:@"http://play.famobi.com/smarty-bubbles"];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:0];
    [self.webView loadRequest:request];
}


#pragma mark - Web View delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    
}

@end
