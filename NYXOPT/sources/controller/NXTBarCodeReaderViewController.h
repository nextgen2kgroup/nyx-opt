//
//  NXTBarCodeReaderViewController.h
//  NYXOPT
//
//  Created by o.koval on 07.09.15.
//  Copyright (c) 2015 o.koval. All rights reserved.
//

#import "QRCodeReaderViewController.h"

@interface NXTBarCodeReaderViewController : QRCodeReaderViewController

@end
