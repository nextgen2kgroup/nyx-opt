//
//  NXTManualInputViewController.m
//  NYXOPT
//
//  Created by o.koval on 8/27/15.
//  Copyright (c) 2015 o.koval. All rights reserved.
//

#import "NXTManualInputViewController.h"

@interface NXTManualInputViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *manualInputTextField;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UILabel *enterBarCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *gamingVoucherValidationLabel;

@end

@implementation NXTManualInputViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.manualInputTextField.delegate = self;
}

#pragma mark - actions
- (IBAction)submitButtonTapped:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(manualInput:didFinishWithResult:)])
    {
        [self.delegate manualInput:self didFinishWithResult:self.manualInputTextField.text];
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)cancelButtonTapped:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(manualInputDidCancel:)])
    {
        [self.delegate manualInputDidCancel:self];
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - UITextField Delegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
@end
