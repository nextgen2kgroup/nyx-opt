//
//  NXTManualInputViewController.h
//  NYXOPT
//
//  Created by o.koval on 8/27/15.
//  Copyright (c) 2015 o.koval. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NXTBaseViewController.h"

@class NXTManualInputViewController;

@protocol NXTManualInputDelegate <NSObject>

@optional

- (void)manualInput:(NXTManualInputViewController *)reader didFinishWithResult:(NSString *)result;

- (void)manualInputDidCancel:(NXTManualInputViewController *)reader;

@end

@interface NXTManualInputViewController : NXTBaseViewController

@property (weak, nonatomic) id<NXTManualInputDelegate> delegate;

@end
