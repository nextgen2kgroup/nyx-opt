//
//  NXTWebViewController.h
//  NYXOPT
//
//  Created by o.koval on 08.09.15.
//  Copyright (c) 2015 o.koval. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NXTBaseViewController.h"

@interface NXTWebViewController : NXTBaseViewController

@end
