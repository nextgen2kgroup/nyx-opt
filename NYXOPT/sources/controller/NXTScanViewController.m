//
//  NXTScanViewController.m
//  NYXOPT
//
//  Created by o.koval on 8/26/15.
//  Copyright (c) 2015 o.koval. All rights reserved.
//

#import "NXTScanViewController.h"
#import "QRCodeReaderViewController.h"
#import "QRCodeReader.h"
#import "NXTManualInputViewController.h"
#import "NXTBarCodeReader.h"
#import "NXTBarCodeReaderViewController.h"

@interface NXTScanViewController () <QRCodeReaderDelegate, NXTManualInputDelegate>
@property (weak, nonatomic) IBOutlet UIButton *scanButton;
@property (weak, nonatomic) IBOutlet UIButton *manualInputButton;
@property (weak, nonatomic) IBOutlet UILabel *gamingVoucherValidationLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectLabel;

@property (strong, nonatomic) NSString *qrCodeString;
@property (strong, nonatomic) NXTManualInputViewController *manualInput;
@end

@implementation NXTScanViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Actions

- (IBAction)scanButtonTapped:(id)sender
{
    static NXTBarCodeReaderViewController *reader = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        reader = [NXTBarCodeReaderViewController new];
        reader.modalPresentationStyle = UIModalPresentationFullScreen;
    });
    reader.delegate = self;
    [reader setCompletionWithBlock:^(NSString *resultAsString) {
    }];
    [self presentViewController:reader animated:YES completion:NULL];
}

- (IBAction)manualInputButtonTapped:(id)sender
{
    
}

#pragma mark - QRCodeReader Delegate Methods

- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result
{
    [self dismissViewControllerAnimated:YES completion:NULL];
    self.qrCodeString = result;
}

- (void)readerDidCancel:(QRCodeReaderViewController *)reader
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - NXTManualInput Delegate Methods

- (void)manualInput:(NXTManualInputViewController *)reader didFinishWithResult:(NSString *)result
{
    self.qrCodeString = result;
    self.manualInput = nil;
}

- (void)manualInputDidCancel:(NXTManualInputViewController *)reader
{
    self.manualInput = nil;
}

#pragma mark -
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isMemberOfClass:[NXTManualInputViewController class]])
    {
        NXTManualInputViewController *manualViewController = (NXTManualInputViewController *)segue.destinationViewController;
        manualViewController.delegate = self;
        self.manualInput = manualViewController;
    }
}

@end
